<?php
/**
 * This file is part of the Magebit PageListWidget package.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magebit PageListWidget
 * to newer versions in the future.
 *
 * @copyright Copyright (c) 2019 Magebit, Ltd. (https://magebit.com/)
 * @license   GNU General Public License ("GPL") v3.0
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Magebit\PageListWidget\Block\Widget;

use Magento\Cms\Model\PageFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Widget\Block\BlockInterface;

/**
 * Class page list block widget
 */
class PageList extends Template implements BlockInterface
{
    /**
     * Widget values
     */
    const TITLE = 'title';
    const SELECTED_PAGES = 'selected_pages';
    const DISPLAY_MODE = 'display_mode';

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $_template = 'page-list.phtml';

    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory
    )
    {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
    }

    /**
     * Get navigation title
     *
     * @return string
     */
    public function getTitle() {
        $this->title = $this->getData(self::TITLE);
        if (!$this->title) {
            $this->title = '';
        }
        return $this->title;
    }

    /**
     * Get pages collection
     *
     * @return array|\Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getPages() {
        $pagesUrl = explode(',', $this->getData(self::SELECTED_PAGES));
        $pages = $this->pageFactory->create()->getCollection();
        if ($this->getData(self::DISPLAY_MODE)) {
            $pages->addFieldToFilter('identifier', array('in' => $pagesUrl));
        }
        return $pages;
    }
}
