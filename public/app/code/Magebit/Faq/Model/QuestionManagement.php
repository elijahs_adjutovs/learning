<?php
/**
 * This file is part of the Magebit Faq package.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magebit Faq
 * to newer versions in the future.
 *
 * @copyright Copyright (c) 2019 Magebit, Ltd. (https://magebit.com/)
 * @license   GNU General Public License ("GPL") v3.0
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Magebit\Faq\Model;

use Magebit\Faq\Api\Data\QuestionInterface;
use Magebit\Faq\Api\QuestionManagementInterface;

/**
 * Class question management
 *
 * @api
 * @since 1.0.0
 */
class QuestionManagement implements QuestionManagementInterface
{
    /**
     * Enable question status
     *
     * @param QuestionInterface $question
     * @return QuestionInterface
     */
    public function enableQuestion(QuestionInterface $question)
    {
        $question->setStatus(true);
        return $question;
    }

    /**
     * Disable question status
     *
     * @param QuestionInterface $question
     * @return QuestionInterface
     */
    public function disableQuestion(QuestionInterface $question)
    {
        $question->setStatus(false);
        return $question;
    }
}
