<?php
/**
 * This file is part of the Magebit Faq package.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magebit Faq
 * to newer versions in the future.
 *
 * @copyright Copyright (c) 2019 Magebit, Ltd. (https://magebit.com/)
 * @license   GNU General Public License ("GPL") v3.0
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Magebit\Faq\Controller\Adminhtml\Question;

use Magebit\Faq\Controller\Adminhtml\Question\Index as Question;
use Magebit\Faq\Model\Question as QuestionModel;
use Magebit\Faq\Api\QuestionRepositoryInterface as QuestionRepository;
use Magento\Backend\App\Action;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Search\Controller\Adminhtml\Synonyms\ResultPageBuilder;

/**
 * Class edit question action
 */
class Edit extends Question
{
    /**
     * Result page factory
     *
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * Question model
     *
     * @var QuestionModel
     */
    protected $questionModel;

    /**
     * @var QuestionRepository
     */
    protected $questionRepository;

    /**
     * @param Action\Context $context
     * @param PageFactory $resultPageFactory
     * @param Registry $registry
     * @param QuestionModel $questionModel
     * @param QuestionRepository $questionRepository
     */
    public function __construct(
        Action\Context $context,
        PageFactory $resultPageFactory,
        Registry $registry,
        QuestionModel $questionModel,
        QuestionRepository $questionRepository
    ) {
        parent::__construct($context, $resultPageFactory);
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $registry;
        $this->questionModel = $questionModel;
        $this->questionRepository = $questionRepository;
    }

    /**
     * Init actions
     *
     * @return \Magento\Framework\View\Result\Page
     */
    protected function initAction() {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magebit_Faq::question')
            ->addBreadcrumb(__('Frequently Asked Question'), __('Frequently Asked Question'));
        return $resultPage;
    }

    /**
     * Edit question action
     *
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->questionModel;
        if($id) {
            $model = $this->questionRepository->getById($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This question no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->coreRegistry->register('faq', $model);

        $resultPage = $this->initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Frequently Asked Question') : __('New Frequently Asked Question'),
            $id ? __('Edit Frequently Asked Question') : __('New Frequently Asked Question')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Frequently Asked Questions'));
        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ? $model->getQuestion() : __('New Frequently Asked Question'));

        return $resultPage;
    }
}
