<?php
/**
 * This file is part of the Magebit Faq package.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magebit Faq
 * to newer versions in the future.
 *
 * @copyright Copyright (c) 2019 Magebit, Ltd. (https://magebit.com/)
 * @license   GNU General Public License ("GPL") v3.0
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Magebit\Faq\Block;

use Magebit\Faq\Model\ResourceModel\Question\CollectionFactory as QuestionCollectionFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class question list block
 */
class QuestionList extends Template
{
    /**
     * @var QuestionCollectionFactory
     */
    protected $questionCollectionFactory;

    /**
     * @param Context $context
     * @param QuestionCollectionFactory $questionCollectionFactory
     */
    public function __construct(
        Context $context,
        QuestionCollectionFactory $questionCollectionFactory
    )
    {
        parent::__construct($context);
        $this->questionCollectionFactory = $questionCollectionFactory;
    }

    /**
     * Get question collection
     *
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getQuestionCollection()
    {
        $questionCollection = $this->questionCollectionFactory->create();
        $questionCollection->addFieldToFilter('status', 1);
        $questionCollection->setOrder('position', 'ASC');
        return $questionCollection;
    }
}
