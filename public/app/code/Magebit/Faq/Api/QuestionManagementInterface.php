<?php
/**
 * This file is part of the Magebit Faq package.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magebit Faq
 * to newer versions in the future.
 *
 * @copyright Copyright (c) 2019 Magebit, Ltd. (https://magebit.com/)
 * @license   GNU General Public License ("GPL") v3.0
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Magebit\Faq\Api;

use Magebit\Faq\Api\Data\QuestionInterface;

/**
 * Magebit Faq management interface
 *
 * @api
 * @since 1.0.0
 */
interface QuestionManagementInterface
{
    /**
     * Enable question status
     *
     * @param QuestionInterface $question
     * @return QuestionInterface
     */
    public function enableQuestion(QuestionInterface $question);

    /**
     * Disable question status
     *
     * @param QuestionInterface $question
     * @return QuestionInterface
     */
    public function disableQuestion(QuestionInterface $question);
}
