<?php
/**
 * This file is part of the Magebit Faq package.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magebit Faq
 * to newer versions in the future.
 *
 * @copyright Copyright (c) 2019 Magebit, Ltd. (https://magebit.com/)
 * @license   GNU General Public License ("GPL") v3.0
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Magebit\Faq\Api;

use Magebit\Faq\Api\Data\QuestionInterface;
use Magebit\Faq\Api\Data\QuestionSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Magebit Faq Question CRUD interface
 *
 * @api
 * @since 1.0.0
 */
interface QuestionRepositoryInterface
{
    /**
     * Get question by ID
     *
     * @param int $id
     *
     * @return QuestionInterface
     */
    public function getById($id);

    /**
     * Save question
     *
     * @param QuestionInterface $question
     *
     * @return QuestionInterface
     */
    public function save(QuestionInterface $question);

    /**
     * Get list with questions by given search criteria
     *
     * @param SearchCriteriaInterface $criteria
     *
     * @return QuestionSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $criteria);

    /**
     * Delete question
     *
     * @param QuestionInterface $question
     *
     * @return bool true on success
     */
    public function delete(QuestionInterface $question);

    /**
     * Delete question by ID
     *
     * @param $id
     *
     * @return bool true on success
     */
    public function deleteById($id);
}
