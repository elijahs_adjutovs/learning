<?php
/**
 * This file is part of the Magebit Faq package.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magebit Faq
 * to newer versions in the future.
 *
 * @copyright Copyright (c) 2019 Magebit, Ltd. (https://magebit.com/)
 * @license   GNU General Public License ("GPL") v3.0
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Magebit\Faq\Api\Data;

/**
 * Magebit Faq Question interface
 *
 * @api
 * @since 1.0.0
 */
interface QuestionInterface
{
    /**
     * Database table name
     */
    const TABLE = 'magebit_faq_question';

    /**#@+
     * Database table field
     */
    const ID = 'id';
    const QUESTION = 'question';
    const ANSWER = 'answer';
    const STATUS = 'status';
    const POSITION = 'position';
    const UPDATED_AT = 'updated_at';
    /**#@-*/

    /**#@+
     * Question status
     */
    const ENABLED = 1;
    const DISABLED = 0;
    /**#@-*/

    /**
     * Get question id
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion();

    /**
     * Get question answer
     *
     * @return string
     */
    public function getAnswer();

    /**
     * Get question status
     *
     * @return int
     */
    public function getStatus();

    /**
     * Get question position
     *
     * @return int
     */
    public function getPosition();

    /**
     * Get timestamp when question was last updated
     *
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set question
     *
     * @param string $question
     *
     * @return QuestionInterface
     */
    public function setQuestion($question);

    /**
     * Set question answer
     *
     * @param string $answer
     *
     * @return QuestionInterface
     */
    public function setAnswer($answer);

    /**
     * Set question status
     *
     * @param int $status
     *
     * @return QuestionInterface
     */
    public function setStatus($status);

    /**
     * Set question position
     *
     * @param int $position
     *
     * @return QuestionInterface
     */
    public function setPosition($position);
}
